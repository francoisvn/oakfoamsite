<?php

function gettrackercode($ver)
{
  return "target=\"_blank\" onclick=\"_gaq.push(['_trackEvent','Download','Version $ver',this.href]);\"";
}

$tags=explode("\n",file_get_contents('https://api.bitbucket.org/1.0/repositories/francoisvn/oakfoam/raw/tip/.hgtags'));
$c=count($tags);

if ($c>1)
{
  $data="";
  $datamenu="";
  $i=0;
  $ver="";
  $v="";

  for ($j=0;$j<3;$j++)
  {
    while ($v==$ver && $i<$c)
    {
      $t=explode(" ",$tags[$c-2-$i]);
      $i++;
      $v=$t[1];
    }
    $ver=$v;

    $vv=substr($v,1);
    $json=json_decode(file_get_contents('https://api.bitbucket.org/1.0/repositories/francoisvn/oakfoam/changesets/'.$v));
    $time=explode(" ",$json->{'utctimestamp'});

    $linux32deb="";
    $linux32tar="";
    $linux64deb="";
    $linux64tar="";
    $windowsexe="";
    $windowszip="";
    if ($handle = opendir('downloads')) {
      while (false !== ($entry = readdir($handle))) {
        if (strpos($entry,"oakfoam_$vv")!==false)
        {
          if (strpos($entry,"_i386.deb")!==false && ($linux32deb=="" || $linux32deb<$entry))
            $linux32deb=$entry;
          if (strpos($entry,"_i386.tar.gz")!==false && ($linux32tar=="" || $linux32tar<$entry))
            $linux32tar=$entry;
          if (strpos($entry,"_amd64.deb")!==false && ($linux64deb=="" || $linux64deb<$entry))
            $linux64deb=$entry;
          if (strpos($entry,"_amd64.tar.gz")!==false && ($linux64tar=="" || $linux64tar<$entry))
            $linux64tar=$entry;
          if (strpos($entry,"_win32.exe")!==false && ($windowsexe=="" || $windowsexe<$entry))
            $windowsexe=$entry;
          if (strpos($entry,"_win32.zip")!==false && ($windowszip=="" || $windowszip<$entry))
            $windowszip=$entry;
        }
      }

      closedir($handle);
    }

    $data.='<p>';
    $data.="Version $vv (released $time[0]):";
    $data.='<ul>';
    if ($linux32deb!="" || $linux32tar!="")
    {
      $data.="<li>Linux 32-bit: ";
      if ($linux32deb!="")
        $data.="<a href=\"downloads/$linux32deb\" ".gettrackercode($vv).">deb</a>";
      if ($linux32deb!="" && $linux32tar!="")
        $data.=", ";
      if ($linux32tar!="")
        $data.="<a href=\"downloads/$linux32tar\" ".gettrackercode($vv).">gz</a>";
      $data.="</li>";
    }
    if ($linux64deb!="" || $linux64tar!="")
    {
      $data.="<li>Linux 64-bit: ";
      if ($linux64deb!="")
        $data.="<a href=\"downloads/$linux64deb\" ".gettrackercode($vv).">deb</a>";
      if ($linux64deb!="" && $linux64tar!="")
        $data.=", ";
      if ($linux64tar!="")
        $data.="<a href=\"downloads/$linux64tar\" ".gettrackercode($vv).">gz</a>";
      $data.="</li>";
    }
    if ($windowsexe!="" || $windowszip!="")
    {
      $data.="<li>Windows: ";
      if ($windowsexe!="")
        $data.="<a href=\"downloads/$windowsexe\" ".gettrackercode($vv).">exe</a>";
      if ($windowsexe!="" && $windowszip!="")
        $data.=", ";
      if ($windowszip!="")
        $data.="<a href=\"downloads/$windowszip\" ".gettrackercode($vv).">zip</a>";
      $data.="</li>";
    }
    $data.='<li>Source code: ';
    $data.="<a href=\"http://bitbucket.org/francoisvn/oakfoam/get/$v.zip\" ".gettrackercode($vv).">zip</a>, ";
    $data.="<a href=\"http://bitbucket.org/francoisvn/oakfoam/get/$v.tar.gz\" ".gettrackercode($vv).">gz</a>, ";
    $data.="<a href=\"http://bitbucket.org/francoisvn/oakfoam/get/$v.tar.bz2\" ".gettrackercode($vv).">bz2</a>";
    $data.='</li>';
    $data.='</ul>';
    $data.='</p>';

    if ($j==0)
    {
      $datamenu.='<a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-download icon-white"></i> Download '.$vv.' <span class="caret"></span></a>';
      $datamenu.='<ul class="dropdown-menu">';
      if ($linux32deb!="")
        $datamenu.="<li><a href=\"downloads/$linux32deb\" ".gettrackercode($vv).">Linux 32-bit (deb)</a></li>";
      if ($linux32tar!="")
        $datamenu.="<li><a href=\"downloads/$linux32tar\" ".gettrackercode($vv).">Linux 32-bit (gz)</a></li>";
      if ($linux64deb!="")
        $datamenu.="<li><a href=\"downloads/$linux64deb\" ".gettrackercode($vv).">Linux 64-bit (deb)</a></li>";
      if ($linux64tar!="")
        $datamenu.="<li><a href=\"downloads/$linux64tar\" ".gettrackercode($vv).">Linux 64-bit (gz)</a></li>";
      if ($windowsexe!="")
        $datamenu.="<li><a href=\"downloads/$windowsexe\" ".gettrackercode($vv).">Windows (exe)</a></li>";
      if ($windowszip!="")
        $datamenu.="<li><a href=\"downloads/$windowszip\" ".gettrackercode($vv).">Windows (zip)</a></li>";
      $datamenu.="<li><a href=\"http://bitbucket.org/francoisvn/oakfoam/get/$v.zip\" ".gettrackercode($vv).">Source (zip)</a></li>";
      $datamenu.="<li><a href=\"http://bitbucket.org/francoisvn/oakfoam/get/$v.tar.gz\" ".gettrackercode($vv).">Source (gz)</a></li>";
      $datamenu.="<li><a href=\"http://bitbucket.org/francoisvn/oakfoam/get/$v.tar.bz2\" ".gettrackercode($vv).">Source (bz2)</a></li>";
      $datamenu.="<li><a href=\"#downloads\">More...</a></li>";
      $datamenu.='</ul>';
    }
  }
  //echo $data;
  file_put_contents('content.downloads.html',$data);
  file_put_contents('content.downloads.menu.html',$datamenu);
}

