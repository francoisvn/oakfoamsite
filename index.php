<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Oakfoam</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        margin-top: 30px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="favicon.png">
    
    <script type="text/javascript">
      if (document.location.hostname.search("oakfoam.com") !== -1) {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-22667808-2']);
        _gaq.push(['_trackPageview']);

        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
      }
    </script>
  </head>

  <body data-spy="scroll">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <div class="container">
      <header class="jumbotron subhead" id="overview">
        <div class="social pull-right">
          <div class="fb-like" data-send="false" data-layout="button_count" data-show-faces="false"></div><br/>
          <div class="g-plusone" data-size="medium"></div><br/>
          <a href="https://twitter.com/share" class="twitter-share-button" data-text="Check out Oakfoam">Tweet</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
        <h1 id=top>
          <img src="img/logo.png"/>
          Oakfoam
        </h1>
        <p class="lead">Computer player for the game of Go</p>
        <div class="subnav">
          <ul class="nav nav-pills">
            <li><a href="#top">Home</a></li>
            <!--li><a href="#getting-started">Getting Started</a></li-->
            <li><a href="#downloads">Downloads</a></li>
            <li><a href="#development">Development</a></li>
            <li><a href="#publications">Publications</a></li>
            <li class="pull-right">
              <div class="btn-group" id="downloads-menu-content">
                <?php include("content.downloads.menu.html"); ?>
              </div>
            </li>
            <li class="pull-right"><a href="http://bitbucket.org/francoisvn/oakfoam" target="_blank">Source</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div>
      </header>

      <div class="row">
        <div class="span8">
          <section id="about">
            <div class="hero-unit">
            <p>
              Oakfoam is an open-source, cross-platform computer player for the ancient game of <a href="http://en.wikipedia.org/wiki/Go_%28game%29">Go</a>.
              It is about <a href="http://www.gokgs.com/graphPage.jsp?user=oakfoam">5k KGS</a> on 19x19, and stronger on smaller boards.
            </p>
            <p>
              Although Oakfoam has a built-in interface you can use after downloading, there is also support for using <a href="http://gogui.sourceforge.net/">GoGui</a> as an advanced interface.
            </p>
            <p>
              Oakfoam is built on a multi-threaded <a href="http://www.mcts.ai/">Monte-Carlo Tree Search</a> engine with various enhancements, which can be customized by altering a large number of parameters.
              Only Chinese area scoring is currently supported.
            </p>
            </div>
          </section>
        </div>
        
        <div class="span4">
          <section id="getting-started">
            <div class="page-header"><h1>Getting Started</h1></div>

            <p>
              To start playing against Oakfoam:
              <ol>
                <li><a href="#downloads">Download</a> and install binaries for your OS.</li>
                <li>Start Oakfoam:
                  <ul>
                    <li>Click the application menu entry if installed from a package (deb or exe), or</li>
                    <li>execute "run.sh" on Linux, or</li>
                    <li>execute "run.bat" on Windows.</li>
                  </ul>
                </li>
                <li>Your browser should open the interface.</li>
                <li>Start playing.</li>
              </ol>
              Alternatively you can use <a href="http://gogui.sourceforge.net/">GoGui</a> for a more advanced interface.
              Refer to the <a href="http://gogui.sourceforge.net/doc">GoGui Documentation</a> for help.
            </p>
          </section>
        </div>
      </div>

      <section id="downloads">
        <div class="page-header"><h1>Downloads</h1></div>

        <?php include("content.downloads.html"); ?>

        <p>Latest source code: <a href="http://bitbucket.org/francoisvn/oakfoam/get/tip.zip" target="_blank" onclick="_gaq.push(['_trackEvent','Download','Latest Source',this.href]);">zip</a>, <a href="http://bitbucket.org/francoisvn/oakfoam/get/tip.tar.gz" target="_blank" onclick="_gaq.push(['_trackEvent','Download','Latest Source',this.href]);">gz</a>, <a href="http://bitbucket.org/francoisvn/oakfoam/get/tip.tar.bz2" target="_blank" onclick="_gaq.push(['_trackEvent','Download','Latest Source',this.href]);">bz2</a></p>

        <p>More downloads are available <a href="http://bitbucket.org/francoisvn/oakfoam/downloads" target="_blank">here</a>.</p>

        <p>
          <a href="https://itunes.apple.com/us/app/nicego/id565290102" target="_blank"><img src="img/appstore.gif"/></a><br/>
          Oakfoam is the engine used in NiceGo, the iOS app available <a href="https://itunes.apple.com/us/app/nicego/id565290102" target="_blank">here</a>.
        </p>
      </section>

      <section id="development">
        <div class="page-header"><h1>Development</h1></div>

        <p>
          Oakfoam is an open-source project.
          It uses C++ as the main programming language.
          Development makes extensive use of the <a href="http://bitbucket.org/francoisvn/oakfoam">Bitbucket repository</a>, including the wiki and issue tracker.
          If you would like to contribute, please read the <a href="http://bitbucket.org/francoisvn/oakfoam/wiki/Contributions">relevant page</a> on the wiki first.
        </p>
        <p>There is an <a href="http://bitbucket.org/francoisvn/oakfoam/rss">RSS feed</a> for the latest changesets.</p>
        <p>Oakfoam is partially supported by the <a href="http://www.nrf.ac.za/">National Research Foundation of South Africa</a>.</p>
      </section>

      <section id="publications">
        <div class="page-header"><h1>Publications</h1></div>

        <p>
          Oakfoam-related publications:
          <ul>
            <li>F. van Niekerk, "<a href="http://leafcloud.com/research/decision-forests-for-computer-go-feature-learning/">Decision Forests for Computer Go Feature Learning</a>," Stellenbosch University, 2014.</li>
            <li>F. van Niekerk, S. Kroon, "<a href="http://leafcloud.com/research/decision-trees-for-computer-go-features/">Decision Trees for Computer Go Features</a>," at CGW2013 at IJCAI, 2013.</li>
            <li>F. van Niekerk, S. Kroon, G.J. van Rooyen and C.P. Inggs, "<a href="http://leafcloud.com/research/monte-carlo-tree-search-parallelisation-for-computer-go/">Monte-Carlo Tree Search Parallelisation for Computer Go</a>," in SAICSIT'12, 2012.</li>
            <li>F. van Niekerk, "<a href="http://leafcloud.com/research/mcts-parallelisation/">MCTS Parallelisation</a>," Stellenbosch University, 2011.</li>
          </ul>
        </p>

        <p>Please send notifications of new publications to the <a href="http://groups.google.com/group/oakfoam">mailing list</a>.
      </section>

      <section id="contact">
        <div class="page-header"><h1>Contact</h1></div>

        <p>
          Feel free to make contact if you have any questions:
          <ul>
            <li><a href="mailto:oakfoam@gmail.com">Email</a></li>
            <li><a href="http://groups.google.com/group/oakfoam">Mailing list</a></li>
            <li><a href="http://bitbucket.org/francoisvn/oakfoam/issues/new">Report bugs</a></li>
          </ul>
        </p>
      </section>

      <div class="footer">
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>Oakfoam is available under the <a href="http://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_.28.22New_BSD_License.22_or_.22Modified_BSD_License.22.29">BSD open-source license</a>.</p>
      </div>
    </div> <!-- /container -->

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript">
      (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      })();
    </script>
  </body>
</html>
